package com.example.email;

import jakarta.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class EmailApplication {

    @Autowired
    private EmailSenderService emailSenderService;

    public static void main(String[] args) {
        SpringApplication.run(EmailApplication.class, args);
    }

//    @EventListener(ApplicationReadyEvent.class)
//    public void triggerEmail() throws MessagingException {
//        emailSenderService.sendMailWithAttachment("apurbarajyashreethapa@gmail.com","hello",
//                "This is email with attachment","F:\\AdBoard System\\2.png");
//    }

}
