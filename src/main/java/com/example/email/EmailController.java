package com.example.email;

import jakarta.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {

    @Autowired
    private EmailSenderService emailSenderService;

    @GetMapping("/")
    public String welcome(){
        return "This is email welcome page";
    }

    @PostMapping(value = "/sendmail")
    public ResponseEntity<?> sendEmail(@RequestBody Email email) throws MessagingException {
        this.emailSenderService.sendMailWithAttachment(email.getToEmail(), email.getBody(), email.getSubject(), email.getAttachment());

        return ResponseEntity.ok("Email send successfully");
    }
}
